USE [ManData]
GO
/****** Object:  StoredProcedure [dbo].[SP_Account_Migration_Selection_And_Cleaning]    Script Date: 3/22/2022 2:15:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Johan van der Meij,>
-- Create date: <Create Date,,>
-- Description:	<Description,,>

-- Log:
	-- 20220322 F. Grooten	Changes based on conversation with Andre:
		-- In part 1, DON't exclude Albert Heijn with cust. id 301634 since IS a customer  : or d.CUSTOMER_ID = '301634'
		-- In part 1, only get customers with Loglink 'isActive' flag = 'Y'				: where a.ACTIVE_FLAG = 'Y'
		-- In part 2, don't look at balances for party tranfers and depot transfers
		-- I removed the hard coded Id lists from the where statements since it made the code difficult to read and had redundancy. Now a temp table at the beginning of the code
		-- changed indentation to make the code easier to read
		
-- =============================================
ALTER PROCEDURE [dbo].[SP_Account_Migration_Selection_And_Cleaning] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF object_id('tempdb..#Temp_authUsers') IS NOT NULL
	DROP TABLE #Temp_authUsers

select authorizedUsers.* 
into #Temp_authUsers
from (
		SELECT		 '1000009172' as CustomerId, 'Geautoriseerde retailers' as Reason
		UNION SELECT '1000002182', 'Geautoriseerde retailers' 
		UNION SELECT '1000013561', 'Geautoriseerde retailers' 
		UNION SELECT '1000009789', 'Geautoriseerde retailers' 
		UNION SELECT '1000009788', 'Geautoriseerde retailers' 
		UNION SELECT '1000011213', 'Geautoriseerde retailers' 
		UNION SELECT '1000003884', 'Geautoriseerde retailers' 
		UNION SELECT '1000002185', 'Geautoriseerde retailers' 
		UNION SELECT '1000002184', 'Geautoriseerde retailers' 
		UNION SELECT '1000002183', 'Geautoriseerde retailers' 
		UNION SELECT '1000002186', 'Geautoriseerde retailers' 
		UNION SELECT '1000002188', 'Geautoriseerde retailers' 
		UNION SELECT '1000013413', 'Geautoriseerde retailers' 
		UNION SELECT '1000011241', 'Geautoriseerde retailers' 
		UNION SELECT '1000014472', 'Geautoriseerde retailers' 
		UNION SELECT '1000012202', 'Geautoriseerde retailers' 
		UNION SELECT '1000002191', 'Geautoriseerde retailers' 
		UNION SELECT '1000002190', 'Geautoriseerde retailers' 
		UNION SELECT '1000013547', 'Geautoriseerde retailers' 
		UNION SELECT '1000013544', 'Geautoriseerde retailers' 
		UNION SELECT '1000013536', 'Geautoriseerde retailers' 
		UNION SELECT '1000013538', 'Geautoriseerde retailers' 
		UNION SELECT '1000013545', 'Geautoriseerde retailers' 
		UNION SELECT '1000013541', 'Geautoriseerde retailers' 
		UNION SELECT '1000014248', 'Geautoriseerde retailers' 
		UNION SELECT '1000013540', 'Geautoriseerde retailers' 
		UNION SELECT '1000013475', 'Geautoriseerde retailers' 
		UNION SELECT '1000002192', 'Geautoriseerde retailers' 
		UNION SELECT '1000010720', 'Geautoriseerde retailers' 
		UNION SELECT '1000002193', 'Geautoriseerde retailers' 
		UNION SELECT '1000002194', 'Geautoriseerde retailers' 
		UNION SELECT '1000002195', 'Geautoriseerde retailers' 
		UNION SELECT '1000002196', 'Geautoriseerde retailers' 
		UNION SELECT '1000002197', 'Geautoriseerde retailers' 
		UNION SELECT '1000002199', 'Geautoriseerde retailers' 
		UNION SELECT '1000002198', 'Geautoriseerde retailers' 
		UNION SELECT '1000002902', 'Geautoriseerde retailers' 
		UNION SELECT '1000009559', 'Geautoriseerde retailers' 
		UNION SELECT '1000009924', 'Geautoriseerde retailers' 
		UNION SELECT '1000009558', 'Geautoriseerde retailers' 
		UNION SELECT '1000012366', 'Geautoriseerde retailers' 
		UNION SELECT '200920', 'Geautoriseerde retailers' 
		UNION SELECT '1000010139', 'Geautoriseerde retailers' 
		UNION SELECT '1000009859', 'Geautoriseerde retailers' 
		UNION SELECT '1000006156', 'Geautoriseerde retailers' 
		UNION SELECT '1000006153', 'Geautoriseerde retailers' 
		UNION SELECT '1000006154', 'Geautoriseerde retailers' 
		UNION SELECT '1000006162', 'Geautoriseerde retailers' 
		UNION SELECT '1000006185', 'Geautoriseerde retailers' 
		UNION SELECT '1000006159', 'Geautoriseerde retailers' 
		UNION SELECT '1000011608', 'Geautoriseerde retailers' 
		UNION SELECT '1000006161', 'Geautoriseerde retailers' 
		UNION SELECT '1000006157', 'Geautoriseerde retailers' 
		UNION SELECT '1000006158', 'Geautoriseerde retailers' 
		UNION SELECT '1000006155', 'Geautoriseerde retailers' 
		UNION SELECT '1000006160', 'Geautoriseerde retailers' 
		UNION SELECT '1000006163', 'Geautoriseerde retailers' 
		UNION SELECT '1000006165', 'Geautoriseerde retailers' 
		UNION SELECT '1000006152', 'Geautoriseerde retailers' 
		UNION SELECT '1000006164', 'Geautoriseerde retailers' 
		UNION SELECT '3602002', 'Geautoriseerde retailers' 
		UNION SELECT '3602001', 'Geautoriseerde retailers' 
		UNION SELECT '3602000', 'Geautoriseerde retailers' 
		UNION SELECT '1000002656', 'Geautoriseerde retailers' 
		UNION SELECT '1000008312', 'Geautoriseerde retailers' 
		UNION SELECT '1000001681', 'Geautoriseerde retailers' 
		UNION SELECT '1000014560', 'Geautoriseerde retailers' 
		UNION SELECT '1000014109', 'Geautoriseerde retailers' 
		UNION SELECT '1000007785', 'Geautoriseerde retailers' 
		UNION SELECT '1000007782', 'Geautoriseerde retailers' 
		UNION SELECT '1000006921', 'Geautoriseerde retailers' 
		UNION SELECT '1000008078', 'Geautoriseerde retailers' 
		UNION SELECT '1000012134', 'Geautoriseerde retailers' 
		UNION SELECT '1000011376', 'Geautoriseerde retailers' 
		UNION SELECT '350199', 'Geautoriseerde retailers' 
		UNION SELECT '350475', 'Geautoriseerde retailers' 
		UNION SELECT '350043', 'Geautoriseerde retailers' 
		UNION SELECT '350477', 'Geautoriseerde retailers' 
		UNION SELECT '1000011868', 'Geautoriseerde retailers' 
		UNION SELECT '1000011867', 'Geautoriseerde retailers' 
		UNION SELECT '1000011869', 'Geautoriseerde retailers' 
		UNION SELECT '1000011870', 'Geautoriseerde retailers' 
		UNION SELECT '1000011871', 'Geautoriseerde retailers' 
		UNION SELECT '1000011872', 'Geautoriseerde retailers' 
		UNION SELECT '1000011873', 'Geautoriseerde retailers' 
		UNION SELECT '1000011874', 'Geautoriseerde retailers' 
		UNION SELECT '1000011875', 'Geautoriseerde retailers' 
		UNION SELECT '1000014476', 'Geautoriseerde retailers' 
		UNION SELECT '350161', 'Geautoriseerde retailers' 
		UNION SELECT '350183', 'Geautoriseerde retailers' 
		UNION SELECT '350527', 'Geautoriseerde retailers' 
		UNION SELECT '350313', 'Geautoriseerde retailers' 
		UNION SELECT '350286', 'Geautoriseerde retailers' 
		UNION SELECT '350147', 'Geautoriseerde retailers' 
		UNION SELECT '1000014397', 'Geautoriseerde retailers' 
		UNION SELECT '350295', 'Geautoriseerde retailers' 
		UNION SELECT '350518', 'Geautoriseerde retailers' 
		UNION SELECT '350412', 'Geautoriseerde retailers' 
		UNION SELECT '350272', 'Geautoriseerde retailers' 
		UNION SELECT '350303', 'Geautoriseerde retailers' 
		UNION SELECT '350503', 'Geautoriseerde retailers' 
		UNION SELECT '1000012592', 'Geautoriseerde retailers' 
		UNION SELECT '1000012786', 'Geautoriseerde retailers' 
		UNION SELECT '1000012784', 'Geautoriseerde retailers' 
		UNION SELECT '1000012787', 'Geautoriseerde retailers' 
		UNION SELECT '1000012789', 'Geautoriseerde retailers' 
		UNION SELECT '1000012785', 'Geautoriseerde retailers' 
		UNION SELECT '350100', 'Geautoriseerde retailers' 
		UNION SELECT '1000040836', 'Geautoriseerde retailers' 
		UNION SELECT '350264', 'Geautoriseerde retailers' 
		UNION SELECT '350496', 'Geautoriseerde retailers' 
		UNION SELECT '350231', 'Geautoriseerde retailers' 
		UNION SELECT '350134', 'Geautoriseerde retailers' 
		UNION SELECT '350022', 'Geautoriseerde retailers' 
		UNION SELECT '350546', 'Geautoriseerde retailers' 
		UNION SELECT '350547', 'Geautoriseerde retailers' 
		UNION SELECT '350548', 'Geautoriseerde retailers' 
		UNION SELECT '1000041225', 'Geautoriseerde retailers' 
		UNION SELECT '1000041221', 'Geautoriseerde retailers' 
		UNION SELECT '1000041219', 'Geautoriseerde retailers' 
		UNION SELECT '1000041222', 'Geautoriseerde retailers' 
		UNION SELECT '1000041225', 'Geautoriseerde retailers' 
		UNION SELECT '1000041224', 'Geautoriseerde retailers' 
		UNION SELECT '1000041223', 'Geautoriseerde retailers' 
		UNION SELECT '350132', 'Geautoriseerde retailers' 
		UNION SELECT '1000040175', 'Geautoriseerde retailers' 
		UNION SELECT '1000040546', 'Geautoriseerde retailers' 
		UNION SELECT '1000041205', 'Geautoriseerde retailers' 
		UNION SELECT '1000012226', 'Geautoriseerde retailers' 
		UNION SELECT '1000012227', 'Geautoriseerde retailers' 
		UNION SELECT '1000012224', 'Geautoriseerde retailers' 
		UNION SELECT '1000012526', 'Geautoriseerde retailers' 
		UNION SELECT '1000012225', 'Geautoriseerde retailers' 
		UNION SELECT '1000041428', 'Geautoriseerde retailers' 
		UNION SELECT '350269', 'Geautoriseerde retailers' 
		UNION SELECT '350285', 'Geautoriseerde retailers' 
		UNION SELECT '350260', 'Geautoriseerde retailers' 
		UNION SELECT '350338', 'Geautoriseerde retailers' 
		UNION SELECT '350522', 'Geautoriseerde retailers' 
		UNION SELECT '350241', 'Geautoriseerde retailers' 
		UNION SELECT '350137', 'Geautoriseerde retailers' 
		UNION SELECT '1000011417', 'Geautoriseerde retailers' 
		UNION SELECT '1000011418', 'Geautoriseerde retailers' 
		UNION SELECT '1000010785', 'Geautoriseerde retailers' 
		UNION SELECT '1000010783', 'Geautoriseerde retailers' 
		UNION SELECT '1000005717', 'Geautoriseerde retailers' 
		UNION SELECT '1000012273', 'Geautoriseerde retailers' 
		UNION SELECT '1000012272', 'Geautoriseerde retailers' 
		UNION SELECT '1000010786', 'Geautoriseerde retailers' 
		UNION SELECT '1000010788', 'Geautoriseerde retailers' 
		UNION SELECT '1000010784', 'Geautoriseerde retailers' 
		UNION SELECT '1000010787', 'Geautoriseerde retailers' 
		UNION SELECT '1000002311', 'Geautoriseerde retailers' 
		UNION SELECT '350304', 'Geautoriseerde retailers' 
		UNION SELECT '1000012541', 'Niet geautoriseerd' 
		UNION SELECT '1000011302', 'Niet geautoriseerd' 
		UNION SELECT '1000012198', 'Niet geautoriseerd' 
		UNION SELECT '1000011593', 'Niet geautoriseerd' 
		UNION SELECT '1000011592', 'Niet geautoriseerd' 
		UNION SELECT '1000011594', 'Niet geautoriseerd' 
		UNION SELECT '1000012204', 'Niet geautoriseerd' 
		UNION SELECT '1000013562', 'Niet geautoriseerd' 
		UNION SELECT '1000012203', 'Niet geautoriseerd' 
		UNION SELECT '1000014086', 'Niet geautoriseerd' 
		UNION SELECT '1000014108', 'Niet geautoriseerd' 
		UNION SELECT '1000014522', 'Niet geautoriseerd' 
		UNION SELECT '1000014084', 'Niet geautoriseerd' 
		UNION SELECT '1000014083', 'Niet geautoriseerd' 
		UNION SELECT '1000014582', 'Niet geautoriseerd' 
		UNION SELECT '1000014581', 'Niet geautoriseerd' 
		UNION SELECT '1000014109', 'Niet geautoriseerd' 
		UNION SELECT '1000040085', 'Niet geautoriseerd' 
		UNION SELECT '1000013409', 'Niet geautoriseerd' 
		UNION SELECT '1000014492', 'Niet geautoriseerd' 
		UNION SELECT '1000006164', 'Coop Retailer CH and NL' 
		UNION SELECT '1000008253', 'Coop Retailer CH and NL' 
		UNION SELECT '30000305999', 'Coop Retailer CH and NL' 
		UNION SELECT '1000006160', 'Coop Retailer CH and NL' 
		UNION SELECT '1000006162', 'Coop Retailer CH and NL' 
		UNION SELECT '1000006163', 'Coop Retailer CH and NL' 
		UNION SELECT '1000006159', 'Coop Retailer CH and NL' 
		UNION SELECT '1000006165', 'Coop Retailer CH and NL' 
		UNION SELECT '1000006161', 'Coop Retailer CH and NL' 
		UNION SELECT '1000006185', 'Coop Retailer CH and NL' 
		UNION SELECT '1000006153', 'Coop Retailer CH and NL' 
		UNION SELECT '1000006156', 'Coop Retailer CH and NL' 
		UNION SELECT '1000006158', 'Coop Retailer CH and NL' 
		UNION SELECT '1000006155', 'Coop Retailer CH and NL' 
		UNION SELECT '1000011474', 'Coop Retailer CH and NL' 
		UNION SELECT '1000014507', 'Coop Retailer CH and NL' 
		UNION SELECT '1000006152', 'Coop Retailer CH and NL' 
		UNION SELECT '1000006154', 'Coop Retailer CH and NL' 
		UNION SELECT '1000006157', 'Coop Retailer CH and NL' 
		UNION SELECT '1000007406', 'Coop Retailer CH and NL' 
		UNION SELECT '1000008238', 'Coop Retailer CH and NL' 
		UNION SELECT '1000011608', 'Coop Retailer CH and NL' 
) as authorizedUsers



	--==============================================================================================================
--===================== Customers part I: All customers from Dynamics Retail =================================
--===============================================================================================================

-- All customers from Dynamics Retail 
-- Excluding customers with "Recip" in the name ==> will become a retailer
-- Excluding PoolService and Stichting VersFust Customers ==> Not needed for migration
-- Excluding customers with "Heijn" in the name ==> Alberth Heijn will become a retailer
-- Excluding customers that are on the authorized and on-authorized list of Retailers for DACH ==> all retailers
-- Excluding COOP CH and COOP NL ==> Will become retailers
-- Flag Is_Retailer from Loglink = "N"
	
	IF object_id('tempdb..#Temp_cust') IS NOT NULL
	DROP TABLE #Temp_cust


select * 
into #Temp_cust
from (
	select a.CUSTOMER_ID
		, d.ORGANIZATIONNAME as Cust_Short_Name
		, a.IS_RETAILER
		, a.ACTIVE_FLAG
		, d.DATAAREAID as Entity
		,'Customer_In_Dynamics_No_Retailer' as Reason_Migrated
	from CC_DSA.LogLink.Customer as a 
	inner join CC_DSA.loglink.Party_Address as b on a.ADDRESS_ID=b.ADDRESS_ID
	inner join CC_DSA.finops.CustCustomerV3 as d on a.CUSTOMER_ID=d.CUSTOMERACCOUNT

	union all
	select  a.CUSTOMER_ID
		, d.ORGANIZATIONNAME as Cust_Short_Name
		, a.IS_RETAILER
		, a.ACTIVE_FLAG
		, d.DATAAREAID as Entity
		,'Customer_In_Dynamics_No_Retailer' as Reason_Migrated
	from CC_DSA.LogLink.CustomerSplitMissing as a 
	inner join CC_DSA.loglink.Party_Address as b on a.ADDRESS_ID=b.ADDRESS_ID
	inner join CC_DSA.finops.CustCustomerV3 as d on a.CUSTOMER_ID=d.CUSTOMERACCOUNT
) as a
where a.IS_RETAILER='N' 
-- Customer name does not have "recip" in the name
and a.CUST_SHORT_NAME not like '%Recip%' 
-- Customer is not from PoolService or Stichting Versfust
and a.CUSTOMER_ID not like '20000%' and a.CUSTOMER_ID not like '21000%' and a.CUSTOMER_ID not like '30000%' and a.CUSTOMER_ID not like '31000%'
--Customer is not Albert Heijn
and (a.CUST_SHORT_NAME  not like '%Heijn%' or a.CUSTOMER_ID = '301634')
-- Customer is not on the list of authorized or non autorisze retailers and is not COOP CH or COOP NL
and a.ACTIVE_FLAG = 'Y'
-- Only get customers which are currently marked as active in loglink
and a.CUSTOMER_ID not in (select CustomerId from #Temp_authUsers)


--========================================================================================================================
--=================== Customers Part II: Balance in Loglink <>0   and not yet in previous selection =====================
--==========================================================================================================================


-- All Customers with balance <>0 on a RETAIL RTI!!!!! that were not yet selected in Part I
-- Excluding customers with "Recip" in the name ==> will become a retailer
-- Excluding PoolService and Stichting VersFust Customers ==> Not needed for migration
-- Excluding customers with "Heijn" in the name ==> Alberth Heijn will become a retailer
-- Excluding customers that are on the authorized and on-authorized list of Retailers for DACH ==> all retailers
-- Excluding COOP CH and COOP NL ==> Will become retailers
-- Flag Is_Retailer from Loglink = "N"

	IF object_id('tempdb..#Active_LL') IS NOT NULL
	DROP TABLE #Active_LL


	select a.CUSTOMER_ID as Customer_id
			, coalesce(d.organizationname, b1.cust_short_Name) as CustomerName
			, coalesce(b.is_retailer,b1.Is_retailer) as Is_Retailer
			, coalesce(b.active_flag,b1.active_flag) as Active_Flag
			, '' as Entity
			, coalesce(pa.country, pa1.country) as Cust_Country
			, coalesce(pa.CITY, pa1.CITY) as Cust_City
			, coalesce(b.ACTIVE_FLAG, b1.active_flag) as LoglinkActive
			, a.RTI_ID as RTI
			, coalesce(c.RTI_SHORT_NAME,c1.RTI_SHORT_NAME) as RTI_Name
			, sum(a.QTY) as CurrentBalance
			, case when d.CUSTOMERACCOUNT is null then 'N' else 'Y' end as IndynamicsRetail
		into #Active_LL
	from CC_DSA.loglink.BALANCE as a
	left join CC_DSA.loglink.Customer as b on a.CUSTOMER_ID=b.CUSTOMER_ID
	left join CC_DSA.LogLink.CustomerSplitMissing as b1 on a.CUSTOMER_ID=b1.CUSTOMER_ID
	left join CC_DSA.finops.CustCustomerV3 as d on a.CUSTOMER_ID=d.CUSTOMERACCOUNT
	left join CC_DSA.salesforce.Account as e on a.CUSTOMER_ID=e.Customer_Number_Axapta__c
	left join CC_DSA.loglink.RTI as c on a.RTI_ID=c.RTI_ID
	left join CC_DSA.loglink.RTI_Missing_Split as c1 on a.RTI_ID=c1.RTI_ID
	left join CC_DSA.LogLink.Party_Address pa on b.ADDRESS_ID = pa.ADDRESS_ID
	left join CC_DSA.LogLink.Party_Address pa1 on b1.ADDRESS_ID = pa1.ADDRESS_ID 
	inner join CC_DSA.split.RTI as rti on a.RTI_ID=rti.RTI_ID and YN='Y'
	where  
		---- Customer is not on the list of authorized or non autorisze retailers and is not COOP CH or COOP NL
		a.CUSTOMER_ID not in (select CustomerId from #Temp_authUsers)
		and (b.IS_RETAILER='N' or b1.IS_RETAILER='N')
		-- Customer name does not have "recip" in the name
		and (b.CUST_SHORT_NAME not like '%Recip%' or b1.CUST_SHORT_NAME not like '%Recip%')
		-- Customer is not from PoolService or Stichting Versfust
		and (a.CUSTOMER_ID not like '20000%' and a.CUSTOMER_ID not like '21000%')
		--Customer is not Albert Heijn
		and (b.CUST_SHORT_NAME  not like '%Heijn%' or b1.CUST_SHORT_NAME  not like '%Heijn%')
	group by  a.CUSTOMER_ID, b.CUST_SHORT_NAME, a.RTI_ID, c.RTI_SHORT_NAME, b1.CUST_SHORT_NAME, d.CUSTOMERACCOUNT, d.ORGANIZATIONNAME, b.IS_RETAILER, b1.IS_RETAILER
		, e.Customer_Number_Axapta__c, b.ACTIVE_FLAG, b1.ACTIVE_FLAG, pa.COUNTRY, pa1.COUNTRY, pa.CITY, pa1.CITY, e.Customer_Number_Axapta__c, e.Segment__c, c1.RTI_SHORT_NAME


--====================== Sum of Balances ===========================================================

	IF object_id('tempdb..#Active_LL_Sum') IS NOT NULL
	DROP TABLE #Active_LL_Sum

	select Customer_id, CustomerName, sum(CurrentBalance) as BalanceSum
		into #Active_LL_Sum
	from #Active_LL
	group by Customer_id, CustomerName
	having sum(currentbalance)<>0


--=====================================================================================================
--=================  Combine selection Part I and Part II  ============================================
--======================================================================================================

	IF object_id('tempdb..#temp_Cust_2') IS NOT NULL
	DROP TABLE #temp_Cust_2

	select a.*  into #temp_Cust_2
	from(
		select * 
		from #Temp_cust
		union all
		select 
			a.Customer_id
			, a.Cust_Short_Name
			, a.Is_Retailer
			, a.Active_Flag
			, case when a.Country in ('NL','BE') then 'NL' else case when a.Country in ('DE', 'CH','AT') then 'DE' else 'AS' end end as Entity
			, a.Reason_Migrated
		from (
				select a.Customer_id
					, a.CustomerName as Cust_Short_Name
					,  coalesce(c.IS_RETAILER, c1.IS_RETAILER) as Is_Retailer
					, coalesce(c.active_Flag, c1.Active_flag) as Active_Flag
					, coalesce(pa.COUNTRY, pa1.country) as Country
					, 'Balance <>0 on Retail RTI and no Retailer' as Reason_Migrated
				from #Active_LL_Sum as a
				left join #Temp_cust as b on a.Customer_id=b.CUSTOMER_ID
				left join CC_DSA.loglink.Customer as c on a.Customer_id=c.CUSTOMER_ID
				left join CC_DSA.loglink.CustomerSplitMissing as c1 on a.Customer_id=c1.CUSTOMER_ID
				left join CC_DSA.LogLink.Party_Address pa on c.ADDRESS_ID = pa.ADDRESS_ID
				left join CC_DSA.LogLink.Party_Address pa1 on c1.ADDRESS_ID = pa1.ADDRESS_ID 
				where b.CUSTOMER_ID is null
			) as a
		) as a


--========================================================================================================================
--=================== Customers Part III: Customers with Active contracts not yet in previous selections =====================
--==========================================================================================================================


-- All Customers with an active contract on a RETAIL RTI!!!!! that were not yet selected in Part I and Part II
-- Excluding customers with "Recip" in the name ==> will become a retailer
-- Excluding PoolService and Stichting VersFust Customers ==> Not needed for migration
-- Excluding customers with "Heijn" in the name ==> Alberth Heijn will become a retailer
-- Excluding customers that are on the authorized and on-authorized list of Retailers for DACH ==> all retailers
-- Excluding COOP CH and COOP NL ==> Will become retailers
-- Flag Is_Retailer from Loglink = "N"


	IF object_id('tempdb..#Active_CTR') IS NOT NULL
	DROP TABLE #Active_CTR

	select a.* 
		into #Active_CTR 
	from (
			select a.INITIATOR--, b.CustomerName
				, d.CUST_SHORT_NAME
				, d.IS_RETAILER
				, b.RTI_ID
				, 'Y' as ActiveContracts
				--	into #Active_CTR
			from cc_dsa.loglink.doc_hdr as a
			inner join CC_DSA.loglink.DOC_LINE as b on a.DOC_ID=b.DOC_ID
			inner join CC_DSA.split.rti as c on b.RTI_ID=c.RTI_ID and c.YN='Y'
			--inner join #Retail_cust as b on a.INITIATOR=b.Customer_ID
			left join CC_DSA.loglink.Customer as d on a.INITIATOR=d.CUSTOMER_ID
			where a.DOC_TYPE='CTR' and a.STATUS not in ('CAN','TER','DNT','DNR','DEN')
				and a.CONTRACT_TYPE in ('FT1W','FT1Y','FT3M','FT6M','GA','LTH', 'PUC', 'DF')
			group by a.INITIATOR, b.RTI_ID,d.CUST_SHORT_NAME, d.IS_RETAILER --, b.CustomerName

			union all
			select a.INITIATOR--, b.CustomerName
				, d.CUST_SHORT_NAME
				, d.IS_RETAILER
					, b.RTI_ID
				, 'Y' as ActiveContracts
				--	into #Active_CTR
			from cc_dsa.loglink.Doc_HDR_Missing_Split as a
			inner join CC_DSA.loglink.DOC_LINE_Missing_Split as b on a.DOC_ID=b.DOC_ID
			inner join CC_DSA.split.rti as c on b.RTI_ID=c.RTI_ID and c.YN='Y'
			left join CC_DSA.loglink.Customer as d on a.INITIATOR=d.CUSTOMER_ID
			--inner join #Retail_cust as b on a.INITIATOR=b.Customer_ID
			where a.DOC_TYPE='CTR' and a.STATUS not in ('CAN','TER','DNT','DNR','DEN')
				and a.CONTRACT_TYPE in ('FT1W','FT1Y','FT3M','FT6M','GA','LTH', 'PUC', 'DF')
			group by a.INITIATOR, b.RTI_ID, d.CUST_SHORT_NAME, d.IS_RETAILER--, b.CustomerName
	) as a
	where  
	---- Customer is not on the list of authorized or non autorisze retailers and is not COOP CH or COOP NL
	a.INITIATOR not in (select * from #Temp_authUsers)
	and (a.IS_RETAILER='N')
	-- Customer name does not have "recip" in the name
	and (a.CUST_SHORT_NAME not like '%Recip%' )
	-- Customer is not from PoolService or Stichting Versfust
	and (a.INITIATOR not like '20000%' )
	--Customer is not Albert Heijn
	and (a.CUST_SHORT_NAME  not like '%Heijn%')

--=========================================================================================================
--============  Add part III to Part I and II  ===============================================================
--==============================================================================================================

	insert into #temp_Cust_2
	select INITIATOR,c.CUST_SHORT_NAME, a.IS_RETAILER, c.ACTIVE_FLAG, case when d.COUNTRY in ('DE', 'CH', 'AT') then 'DE' 
		else case when d.COUNTRY in ('NL','BE') then 'NL' else 'AS' end end as Entity
		, 'Active_retail_RTI_Contract' as Reason_Migrated
	from #Active_CTR as a
	left join #temp_Cust_2 as b on a.INITIATOR=b.CUSTOMER_ID
	left join CC_DSA.loglink.Customer as c on a.INITIATOR=c.CUSTOMER_ID
	left join CC_DSA.loglink.Party_Address as d on c.ADDRESS_ID=d.ADDRESS_ID
	where b.CUSTOMER_ID is null
		and a.INITIATOR not like '21000%' 
	group by INITIATOR, b.CUSTOMER_ID, c.CUST_SHORT_NAME, a.IS_RETAILER,c.ACTIVE_FLAG, d.country


--========================================================================================================================
--=================== Customers Part IV: Customers with a trancations after 01-04-2017  =================================
--==========================================================================================================================


-- All Customers with a transaction on a RETAIL RTI!!!!! after april 1 2017 that was not yet selected in Part I, Part II adn Part III
-- Excluding customers with "Recip" in the name ==> will become a retailer
-- Excluding PoolService and Stichting VersFust Customers ==> Not needed for migration
-- Excluding customers with "Heijn" in the name ==> Alberth Heijn will become a retailer
-- Excluding customers that are on the authorized and on-authorized list of Retailers for DACH ==> all retailers
-- Excluding COOP CH and COOP NL ==> Will become retailers
-- Flag Is_Retailer from Loglink = "N"


--============== Make temp table with all transactions on Retail RTI's  ==========================================

	 IF object_id('tempdb..#trans_LL') IS NOT NULL
	DROP TABLE #Trans_LL

select * 
	into #trans_LL
from 
	(
		select a.CUSTOMER_ID
			, coalesce(e.CUST_SHORT_NAME,f.CUST_SHORT_NAME) as Cust_Short_Name
			, coalesce(e.IS_RETAILER, f.IS_RETAILER) as Is_Retailer
			, cast(a.TRANS_DATE as date) as Trans_date
			, a.TRANS_TYPE
			, b.RTI_ID
			, d.RTI_SHORT_NAME
			, b.QTY 
		from CC_DSA.loglink.TRANS_HDR as a
		inner join CC_DSA.loglink.TRANS_LINE as b on a.TRANS_ID=b.TRANS_ID
		inner join CC_DSA.split.RTI as c on b.RTI_ID=c.RTI_ID and c.YN='Y'
		left join CC_DSA.loglink.RTI as d on b.RTI_ID=d.RTI_ID
		left join CC_DSA.LogLink.Customer as e on a.CUSTOMER_ID=e.CUSTOMER_ID
		left join CC_DSA.LogLink.CustomerSplitMissing as f on a.CUSTOMER_ID=f.CUSTOMER_ID

		union all
		select a.CUSTOMER_ID
			, coalesce(e.CUST_SHORT_NAME,f.CUST_SHORT_NAME) as Cust_Short_Name
			, coalesce(e.IS_RETAILER, f.IS_RETAILER) as Is_Retailer
			, cast(a.TRANS_DATE as date) as Trans_date
			, a.TRANS_TYPE
			, b.RTI_ID
			, d.RTI_SHORT_NAME
			, b.QTY 
		from CC_DSA.loglink.TRANS_HDR_Missing_Split as a
		inner join CC_DSA.loglink.TRANS_LINE_Missing_Split as b on a.TRANS_ID=b.TRANS_ID
		inner join CC_DSA.Split.RTI as c on b.RTI_ID=c.RTI_ID and c.YN='Y'
		left join CC_DSA.loglink.RTI as d on b.RTI_ID=d.RTI_ID
		left join CC_DSA.LogLink.Customer as e on a.CUSTOMER_ID=e.CUSTOMER_ID
		left join CC_DSA.LogLink.CustomerSplitMissing as f on a.CUSTOMER_ID=f.CUSTOMER_ID
	) as a
	where  
	---- Customer is not on the list of authorized or non autorisze retailers and is not COOP CH or COOP NL
	a.CUSTOMER_ID not in (select CustomerId from #Temp_authUsers)
	and (a.IS_RETAILER='N')
	-- Customer name does not have "recip" in the name
	and (a.CUST_SHORT_NAME not like '%Recip%' )
	-- Customer is not from PoolService or Stichting Versfust
	and (a.CUSTOMER_ID not like '20000%' )
	--Customer is not Albert Heijn
	and (a.CUST_SHORT_NAME  not like '%Heijn%')


--=============== Get max transdate from all transactions per customer/rti combination

 
	IF object_id('tempdb..#MaxTrans_LL_N') IS NOT NULL
	DROP TABLE #maxtrans_LL_N

	select a.CUSTOMER_ID, a.RTI_ID,max(a.MaxTrans) as MaxTrans--, a.TRANS_TYPE
		into #maxtrans_LL_N
	from (
		 select a.CUSTOMER_ID
		 , a.RTI_ID
		 --, a.TRANS_TYPE
		 , max(cast(a.trans_date as date)) as MaxTrans
		 from #trans_LL as a
		 --where CUSTOMER_ID='1000001554'
		 group by a.CUSTOMER_ID, a.RTI_ID--, a.TRANS_TYPE
		) as a
	where a.MaxTrans>'2017-04-01'
	group by a.CUSTOMER_ID, a.RTI_ID--, a.TRANS_TYPE


 
 --============ Make table with max transdate per customer and select only customers =============================
 
	IF object_id('tempdb..#MaxTrans_ADD') IS NOT NULL
	DROP TABLE #maxtrans_ADD

 select a.CUSTOMER_ID, c.CUST_SHORT_NAME, c.IS_RETAILER, c.ACTIVE_FLAG
	 ,case when d.COUNTRY in ('DE', 'CH', 'AT') then 'DE' 
	else case when d.COUNTRY in ('NL','BE') then 'NL' else 'AS' end end as Entity
	 , 'RTI Transactions since 01-04-2017' as Migration_Reason
	 into #maxtrans_ADD
 from #maxtrans_LL_N as a
 left join #temp_Cust_2 as b on a.CUSTOMER_ID=b.CUSTOMER_ID
 left join CC_DSA.loglink.Customer as c on a.CUSTOMER_ID=c.CUSTOMER_ID
 left join CC_DSA.loglink.Party_Address as d on c.ADDRESS_ID=d.ADDRESS_ID
 where b.CUSTOMER_ID is null
	and a.CUSTOMER_ID not in ('1000010518','3600445','3600555')
	--Jumbo supermarkten (1000010518 uitgesloten ==> groen
	-- 3600445 Migros Zurich uitgesloten
	-- 3600555 Coop Fachstelle Logistik uitgesloten
 group by a.CUSTOMER_ID, c.CUST_SHORT_NAME, c.IS_RETAILER, c.ACTIVE_FLAG, d.COUNTRY


--================================================================================================
--============= insert manually added customers to the selection ===============================
--=====================================================================================================

insert into #temp_Cust_2
select a.CustomerID 
	, coalesce(b.cust_short_name, c.Cust_short_name) as Cust_short_name
	, coalesce(b.is_retailer, c.Is_retailer) as Is_Retailer
	, coalesce(b.Active_flag, c.active_flag) as Active_Flag
	, case when coalesce(d.COUNTRY, e.country) in ('DE', 'CH') then 'DE' else case when coalesce(d.country,e.country) in ('NL','BE') then 'NL' else 'AS' end end as Entity
	, a.Reason
from mandata.CustomerAdding as a
left join CC_DSA.loglink.Customer as b on a.CustomerID=b.CUSTOMER_ID
left join CC_DSA.loglink.CustomerSplitMissing as c on a.CustomerID=c.CUSTOMER_ID
left join CC_DSA.loglink.Party_Address as d on b.ADDRESS_ID=d.ADDRESS_ID
left join CC_DSA.loglink.Party_Address as e on c.ADDRESS_ID=e.ADDRESS_ID




--=====================================================================================================
--====================  Make table in DWH with Selection  =============================================
--====================================================================================================


truncate table mandata.mandata.MigrationCustomerSelection
--drop table mandata.mandata.MigrationCustomerSelection


	insert into [mandata].[mandata].[MigrationCustomerSelection]
	select a.*
		, coalesce(b.LOGISTIC_EMAIL,c.LOGISTIC_EMAIL) as Email
		, coalesce(e.phone_no,f.phone_no) as Phone
		, case when d.CUSTOMERACCOUNT is null then 'NO' else 'Yes' end as 'Wel_NietInD365Retail'
		, case when d.SALESCURRENCYCODE is null then coalesce(b.base_currency,c.base_currency) else d.SALESCURRENCYCODE end as Currency
		, case when d.INVOICEADDRESSCOUNTRYREGIONISOCODE is null then coalesce(e.COUNTRY, f.country) else d.INVOICEADDRESSCOUNTRYREGIONISOCODE end as InvoiceCountry
		, case when d.DELIVERYADDRESSCOUNTRYREGIONISOCODE is null then coalesce(e.COUNTRY, f.country) else d.DELIVERYADDRESSCOUNTRYREGIONISOCODE end as DeliveryCountry
		, '' as InvoiceTransportInRental
		, getdate() as DW_Inserttime
		--into [mandata].[mandata].[MigrationCustomerSelection]
	 from (
			select * from #temp_Cust_2
			union all 
			select * from #maxtrans_ADD
		) as a
	left join CC_DSA.loglink.Customer as b on a.CUSTOMER_ID=b.CUSTOMER_ID
	left join CC_DSA.loglink.CustomerSplitMissing as c on a.CUSTOMER_ID=c.CUSTOMER_ID
	left join CC_DSA.finops.CustCustomerV3 as d on a.CUSTOMER_ID=d.CUSTOMERACCOUNT and d.DATAAREAID not in ('PS','SVF')
	left join CC_DSA.loglink.Party_Address as e on b.ADDRESS_ID=e.ADDRESS_ID
	left join CC_DSA.loglink.Party_Address as f on c.ADDRESS_ID=f.ADDRESS_ID



 --select * from #trans_LL where CUSTOMER_ID='309958' and Trans_date>'2017-04-01'

--===================================================================================================================
--=========  Temp balance and temp trans for all customers  ========================================================
--=================================================================================================================== 

	 IF object_id('tempdb..#temp_Balance') IS NOT NULL
	DROP TABLE #temp_Balance

	select a.CUSTOMER_ID, sum(a.QTY) as Balance 
		into #temp_Balance
	from CC_DSA.loglink.BALANCE as a
	group by a.CUSTOMER_ID


	IF object_id('tempdb..#MaxTrans_LL_Total') IS NOT NULL
	DROP TABLE #maxtrans_LL_total

	select b.CUSTOMER_ID, max(b.TRANS_DATE) as Max_date
		into #maxtrans_LL_total
	from (
			select a.CUSTOMER_ID, cast(a.TRANS_DATE as date) as Trans_Date from CC_DSA.loglink.TRANS_HDR as a
			union all
			select a.CUSTOMER_ID, cast(a.TRANS_DATE as date) as Trans_Date from CC_DSA.loglink.TRANS_HDR_Missing_Split as a
	) as b
	group by b.CUSTOMER_ID

--=================================================================================================================================================
--=======================  All customers that were NOT selected to migratie, including reason ======================================================
--=================================================================================================================================================



truncate table mandata.mandata.Migration_NOT_Selected
--drop table mandata.mandata.Migration_NOT_Selected


insert into mandata.mandata.Migration_NOT_Selected
select a.*
	, isnull(sfa.Segment__c,'') as 'Old_Salesforce_Segment'
	, case when authusers.CUSTOMER_ID is not null   then authusers.Reason else 
		case when a.CUST_SHORT_NAME like '%Recip%' then 'Recipient_Will_be_part_of_Retailer_setup' else 
			case when a.CUST_SHORT_NAME like '%Heijn%' then 'AlbertHeijn_will_Be_Part_Of_Retailer_Setup' else 
				case when a.IS_RETAILER='Y' then 'Loglink_Setup_Retailer_=_Yes' else 
					case when a.CUSTOMER_ID like '20000%' or a.CUSTOMER_ID like '21000%' or a.CUSTOMER_ID like '30000%' or a.CUSTOMER_ID like '31000%' then 'PoolService_or_SVF_Customer' else ''
					end 
				end 
			end 
		end 
	end as Reason_Excluding
	, isnull(tb.Balance,0) as TotalBalance_PossibleReasonExlcuding
	, case when convert(date,cast(dateadd(day,-1,trans.Max_date) as date)) is null 
	then '' else  convert(varchar(10),cast(dateadd(day,-1,trans.Max_date) as date),121) end  as Last_Transaction_Date_PossibleReasonExluding
	, getdate()

--, b.CUSTOMER_ID 

--into mandata.mandata.Migration_NOT_Selected

from (
		select a.CUSTOMER_ID, a.CUST_SHORT_NAME, a.ACTIVE_FLAG, a.IS_RETAILER
		from CC_DSA.loglink.Customer as a

		union all
		select b.CUSTOMER_ID, b.CUST_SHORT_NAME, b.ACTIVE_FLAG, b.IS_RETAILER
		from CC_DSA.loglink.CustomerSplitMissing as b
) as a
left join [Mandata].[MigrationCustomerSelection] as b on a.CUSTOMER_ID=b.CUSTOMER_ID
left join CC_DSA.salesforce.Account as sfa on a.CUSTOMER_ID=sfa.Customer_Number_Axapta__c
left join #temp_Balance as tb on a.CUSTOMER_ID=tb.CUSTOMER_ID
left join #maxtrans_LL_total as trans on a.CUSTOMER_ID=trans.CUSTOMER_ID
left join #Temp_authUsers as authusers on authusers.customerid = a.customer_id
where b.CUSTOMER_ID is null
	and a.CUSTOMER_ID not like '20000%' and a.CUSTOMER_ID not like '21000%' and a.CUSTOMER_ID not like '30000%' and a.CUSTOMER_ID not like '31000%'

--=========================================================================================
--========= Make Cleaning proposal ===============================================================
--=================================================================================================


	IF object_id('tempdb..#Dyn_Trans') IS NOT NULL
	DROP TABLE #Dyn_Trans


select DATAAREAID,accountnum, min(CREATEDDATETIMECC) as CreatedDate , min(VOUCHER) as Voucher
	Into #Dyn_Trans
from CC_DSA.finops.CC_CustTransEntityBIv3
where DATAAREAID not in ('PS','SVF')
group by accountnum, DATAAREAID



	IF object_id('tempdb..#Temp_Clean') IS NOT NULL
	DROP TABLE #temp_Clean

select a.CUSTOMER_ID
	, a.Cust_Short_Name
	, b.Cust_Short_Name_clean
	, a.Entity
	, a.ACTIVE_FLAG
	, a.Reason_Migrated
	, b._score
	, case when a.Reason_Migrated='Customer_In_Dynamics_No_Retailer' and c.Voucher is null then 'YES' else
	case when a.Reason_Migrated<>'Customer_In_Dynamics_No_Retailer' then 'Not in Dynamics Yet' else
	 'NO' end  end as CanBeDeletedFromDynamicsYN
	, isnull(c.Voucher,'') as CreatedVoucher
	, case when convert(date,cast(c.CreatedDate as date)) is null 
	then '' else  convert(varchar(10),cast(c.CreatedDate as date),121) end as CreatedDate_Voucher
	, tr.Max_date as Max_Trans_date
	, isnull(ctr.ActiveContracts,'') as ActiveContracts_YN
	, isnull(dd.Balance,0) as Current_Balance

	into #Temp_Clean
 from mandata.[Mandata].[MigrationCustomerSelection] as a
left join mandata.[Mandata].[Migration_Cleaning] as b on a.CUSTOMER_ID=b.CUSTOMER_ID
left join #Dyn_Trans as c on a.CUSTOMER_ID=c.ACCOUNTNUM
left join #maxtrans_LL_total as tr on a.CUSTOMER_ID=tr.CUSTOMER_ID
left join #Active_CTR as ctr on a.CUSTOMER_ID=ctr.INITIATOR
left join #temp_Balance as dd on a.CUSTOMER_ID=dd.CUSTOMER_ID

group by a.CUSTOMER_ID, a.Cust_Short_Name
	, a.Entity, a.ACTIVE_FLAG, a.IS_RETAILER
	, a.Reason_Migrated, b._score
	, b._Similarity_Cust_Short_Name
	, b.Cust_Short_Name_clean
	, a.Reason_Migrated
	, c.Voucher
	, c.CreatedDate
	, tr.Max_date
	, ctr.ActiveContracts
	, dd.Balance





truncate table mandata.mandata.Migration_Cleaning_Proposal
--drop table mandata.mandata.Migration_Cleaning_Proposal

insert into mandata.mandata.Migration_Cleaning_Proposal
select a.* 
	, case when a.CanBeDeletedFromDynamicsYN='Yes' and (a.Max_Trans_date<'2017-04-01' or a.Max_Trans_date is null) and a.ActiveContracts_YN='' and a.Current_Balance=0
	then 'DeleteCustomer' else 'MigrateCustomer' end as Proposal
	,getdate()

	--into mandata.mandata.Migration_Cleaning_Proposal
from #Temp_Clean as a
order by a.Cust_Short_Name_clean asc


--=========================================================================================================
--=====================  Delivery Addresses for Locations  ==============================================
--========================================================================================================

	IF object_id('tempdb..#Addresses') IS NOT NULL
	DROP TABLE #Addresses


select a.CUSTOMER_ID
	, a.Cust_Short_Name
	, coalesce(b.ADDRESS_ID, c.address_id) as Address_ID
	into #Addresses
from mandata.mandata.MigrationCustomerSelection as a
left join CC_DSA.loglink.Customer as b on a.CUSTOMER_ID=b.CUSTOMER_ID
left join CC_DSA.loglink.CustomerSplitMissing as c on a.CUSTOMER_ID=c.CUSTOMER_ID

	IF object_id('tempdb..#Addresses2') IS NOT NULL
	DROP TABLE #Addresses2


select a.CUSTOMER_ID
	, a.Cust_Short_Name
	, b.ADDRESS_ID as Location_ID
	, b.COUNTRY 
	, isnull(b.CITY,'') as City
	, isnull(b.STREET_AND_NO,'') as Street
	, isnull(b.ZIP,'') as Zip
	, isnull(b.PHONE_NO,'') as Phone
	, Isnull(b.EMAIL,'') as Email
	--, c.LOCATION_EMAIL
	--, c.ACTIVE_FLAG
	into #Addresses2
from #Addresses as a
left join CC_DSA.loglink.Party_Address as b on a.Address_ID=b.PARTY_ID
where a.Address_ID<>b.ADDRESS_ID
order by a.CUSTOMER_ID asc

drop table mandata.mandata.Migration_Delivery_Addresses

select a.*
	, isnull(b.LOC_SHORT_NM,'') as LocationName
	, isnull(b.LOCATION_EMAIL,'') as Location_Email
	, isnull(b.ACTIVE_FLAG,'') as Active_YN
	--, b.CC_CONTACT_NUMBER 
	into mandata.mandata.Migration_Delivery_Addresses
from #Addresses2 as a
left join CC_DSA.loglink.Test as b on a.CUSTOMER_ID=b.PARTY_ID and a.Location_ID=b.ADDRESS_ID
where a.CUSTOMER_ID<>b.ADDRESS_ID
--and a.CUSTOMER_ID='1000001040'



--============= Clean up temptables ============================
IF object_id('tempdb..#Temp_cust') IS NOT NULL
	DROP TABLE #Temp_cust
 IF object_id('tempdb..#Active_LL') IS NOT NULL
	DROP TABLE #Active_LL
IF object_id('tempdb..#Active_LL_Sum') IS NOT NULL
	DROP TABLE #Active_LL_Sum
IF object_id('tempdb..#temp_Cust_2') IS NOT NULL
	DROP TABLE #temp_Cust_2
IF object_id('tempdb..#Active_CTR') IS NOT NULL
	DROP TABLE #Active_CTR
IF object_id('tempdb..#trans_LL') IS NOT NULL
	DROP TABLE #Trans_LL
IF object_id('tempdb..#MaxTrans_LL_N') IS NOT NULL
	DROP TABLE #maxtrans_LL_N
IF object_id('tempdb..#MaxTrans_ADD') IS NOT NULL
	DROP TABLE #maxtrans_ADD
IF object_id('tempdb..#temp_Balance') IS NOT NULL
	DROP TABLE #temp_Balance
IF object_id('tempdb..#MaxTrans_LL_Total') IS NOT NULL
	DROP TABLE #maxtrans_LL_total
IF object_id('tempdb..#Dyn_Trans') IS NOT NULL
	DROP TABLE #Dyn_Trans
IF object_id('tempdb..#Temp_Clean') IS NOT NULL
	DROP TABLE #temp_Clean


		 IF object_id('tempdb..#Addresses') IS NOT NULL
	DROP TABLE #Addresses
	
			 IF object_id('tempdb..#Addresses2') IS NOT NULL
	DROP TABLE #Addresses2
	IF object_id('tempdb..#Temp_authUsers') IS NOT NULL
	DROP TABLE #Temp_authUsers


END
